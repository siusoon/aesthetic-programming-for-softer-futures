[[_TOC_]]

# Day 1 of Aesthetic Programming for SOFTer Futures

🧠Mon (19.9) - 13.00-16.00

⏰13.00-14.00:
Welcome in "Aula D 1.04" by Nicolaj van der Meulen and Sophie Stephanie Kellner

⏰14.00-16.00:

## 😀1. Who + What + learn

![](https://user-content.gitlab-static.net/a58a123a63cb0535a3eff79beedc6fafca0f318b/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f3133374561523476414f436e31532f736f757263652e676966)

- 😀Something about [me](http://www.siusoon.net)
- 😄Something about teaching / research / art making / coding
- 🙂Something about queering art and technology
- 😉Something about the style and format

**Aesthetic + Programming + for + SOFTer + futures**

> It takes a particular interest in power relations that are relatively under-acknowledged in technical subjects, concerning class and capitalism, gender and sexuality, as well as race and the legacies of colonialism. This is not only related to the politics of representation but also nonrepresentation  (Soon & Cox 2020 - Aesthetic Programming)

**⁉️Speculative question⁉️**

1. Guiding question: What might be SOFTer Digital Futures look like if we focus more on love, care and community?

**[p5.js community statement](https://github.com/processing/p5.js/blob/main/CODE_OF_CONDUCT.md)**

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.iCD9rhbgFQTF8lNNTVY7CQAAAA%26pid%3DApi&f=1)

❤️‍ We are a community of, and in solidarity with, people from every gender identity and expression, sexual orientation, race, ethnicity, language, neuro-type, size, disability, class, religion, culture, subculture, political opinion, age, skill level, occupation, and background.

❤️‍ We acknowledge that not everyone has the time, financial means, or capacity to actively participate, but we recognize and encourage involvement of all kinds. We facilitate and foster access and empowerment. We are all learners.

## 👶🏼2. Knowing each other

- What can you think of when you see/read/feel the term ‘SOFT’?
  - Write it here on the shared pad: https://pad.vvvvvvaria.org/SOFT

- 🥶ice-breaking (5 mins prep)
  1. What is your name and discipline, and interest
  2. Do you have any coding experience, what's that? (it's fine if you don't)
  3. Why do you want to join this course?
  4. Do you have anything to share regarding your prep homework? (readings/animated gif)

## ✨3. Coding Otherwise - the notion of SOFTer Futures

<img src="https://lovelanguages.melaniehoff.com/wp-content/themes/dlltheme/img/dll2022.png" width="350">

❤️‍[Digital Love Languages](https://lovelanguages.melaniehoff.com/) by Melanie Hoff

<img src="https://i.imgur.com/O0NPEbK.jpg" width="350">

❤️‍[Softer Digital Futures](https://softerdigitalfutures.xyz/) by [Softer](https://softer.website/)(Ida Lissner and Nicole Jonasson)
  - Other [soft networks](https://softnet.works/)

<img src="https://queeringdamage.hangar.org/images/6/65/Fotoactivitat.jpeg" width="350">

❤️‍[Queering Damage](https://queeringdamage.hangar.org/index.php/Main_Page) by The Underground Division

> we propose that we might pay attention to the damage, injury, harms and constraints placed on the possibilities of life and brought about through informatics/technosciences.

> Damage - We would like to widen the notion of damage and identify its conditions and surrounding through examples, practices things we come into contact with, and by damage we mean things like: pain, suffering, injury, uselessness, unrevoerability, unreparability, homophobia, racism, ageism, ableims, specism, classism, exclusions, inclusions

> Queering Methodologies -- Methodologies: Focus on how we generate data, knowledge, stories, as part of feminist research practices.[...] how can we, as Octavia Butler might say, uphold a politics of queer survival? 

examples of damage: https://queeringdamage.hangar.org/index.php/OPERATION_1

## 💻4. p5.js and the web editor
- Walkthrough [p5.js](https://p5js.org/)
- The [web editor](https://editor.p5js.org/)
- Aesthetic Programming [Showcase](https://aesthetic-programming.net/pages/showcase.html)
- [Sketches](https://editor.p5js.org/siusoon/collections/zcD9tWrlt) for this week

## 📖5. Homework (due tomorrow at 0900)
  - Forming groups - 3 to 4 people in a group (https://pad.vvvvvvaria.org/SOFT)
  - **Read** the assigned reading for tomorrow

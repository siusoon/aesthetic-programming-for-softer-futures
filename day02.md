[[_TOC_]]

# Day 2 of Aesthetic Programming for SOFTer Futures

👂👁Tue (20.9) - 09.00-16.00: Queering damage

Recall: [ellipses](https://editor.p5js.org/siusoon/sketches/QLDeluscp)

## 1. Basics:

### ✨1.1 Coordinate System

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/1-GettingStarted/ch1_11.png" width="600">

- a way to determine the position of things > involves identification and ordering (based on numbers and mathemathics)
- 👀 frame of reference > origin > perspective > ways of seeing

- `createCanvas(400, 400);`
- `createCanvas(windowWidth,windowHeight);`
- `ellipse(200,200,10,10);`

### ✨1.2 two important functions in p5.js

The concepts of time, draw, pixel, canvas, sketch

- `function setup()` → Code within this function will only be "run once" by the sketch work file. This is typically used to set the canvas size to define the basic sketch setup.

- `function draw()` → Taking cues from drawing practice in visual arts, code within this function will keep on looping, and that means function draw() is called on for each running frame. The default rate is 60 frames/times per second

- the symbol `//` (comments): referring to text that are written for humans but not computers. This means a computer will automatically ignore those code comments when it executes the code. or /*……………*/: indicates multiple lines of code comments.

💡note: keep a good practice of commenting your code

### 1.3 Reading the reference guide together

https://p5js.org/reference/

Some terminologies: [color](https://p5js.org/reference/#/p5/color), parameters, values, variables, console.log

🔖resource:
- [Color Picker](https://htmlcolorcodes.com/color-picker/)

### 👾1.4 in-class ex 1:

A) Try to explore the [reference](https://p5js.org/reference/) page, select some syntaxes and change the values of the sample code's parameters.

B) Find and understand 2 new syntaxes, then use them and make a sketch about "soft" with the p5 web editor.

## ✨2 Textuality

### 2.1 `text()`

💻RunMe: https://editor.p5js.org/siusoon/sketches/YucNlbqBT

```javascript
function setup() {
  createCanvas(400, 400);
  //only draw once
  noLoop();
}
function draw() {
  background(255,255,0);
  //no outline
  noStroke();
  //fill color
  fill(255, 0, 255);
  //write the text on the canvas
  text("Hello World!", 50, 200);
}
```

💡note: default font size is 12 pixels

💡you can add `textSize(40);`

### 2.2 to play with size and color with the draw() and interactive functions

💻RunMe: https://editor.p5js.org/siusoon/sketches/F9EyyaPh8

```javascript
function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(255,255,0);
  textSize(30);
  //no outline
  noStroke();
  //fill color
  fill(255, 0, 255);
  //using the sine and frameCount functions to calculate a variable value
  let c = 150 + sin(frameCount*0.1) * 105;
  // color in RGB values
  fill(c, 0 , c);
  //write the text on the canvas
  text("Hello World!", mouseX, mouseY);
}
```

🔖resource:
- [Sine function](https://www.math.net/sine)
- variable: a temporary storage in which the value can be changed, and reuse in your program.
  - To **declare** a variable c we use "let c"
  - If you have already declared a variable, then use the `=` to **assign** value e.g `c = 100;`
  - You can also **declare and assign** the variable and its value (at the same time): `let c = 100;`

💡note:
- Sine is a period function, returning values between -1 to +1

### ✨2.3 text alignment

💻RunMe: https://editor.p5js.org/siusoon/sketches/K7l5_dW8R

```javascript
function setup() {
  createCanvas(400, 400);
  noLoop();
}
function draw() {
  background(255,255,0);
  textSize(28);

  // left
  fill(0, 255, 0);
  ellipse(200, 100, 10, 10);
  fill(255, 0, 255);
  textAlign(LEFT);
  text("Hello World!", 200, 100);

  // center
  fill(0, 255, 0);
  ellipse(200, 200, 10, 10);
  fill(255, 0, 255);
  textAlign(CENTER);
  text("Hello World!", 200, 200);

  // right
  fill(0, 255, 0);
  ellipse(200, 300, 10, 10);
  fill(255, 0, 255);
  textAlign(RIGHT);
  text("Hello World!", 200, 300);
}
```

### ✨2.4 text in a box

💻RunMe: https://editor.p5js.org/siusoon/sketches/OukTIPt-S

```JavaScript
function setup() {
  createCanvas(400, 400);
}
function draw() {
  background(255,255,0);
  fill(255, 0, 255);
  textSize(22);
  text("What is soft means to you, and how to express that?",
    mouseX, mouseY, 200, 200);
  if (mouseIsPressed) {
    noFill();
    rect(mouseX, mouseY, 200, 200);
  }
}
```

### ✨2.5 text leading

💻RunMe: https://editor.p5js.org/siusoon/sketches/ufKjpVnw5

```javascript
function setup() {
  createCanvas(400, 400);
}
function draw() {
  background(255,255,0);
  fill(255,0,255);
  textSize(22);
  //move your mouse (spacing, in pixels, between lines of text)
  textLeading((mouseY / height) * 80);
  // "\n" new line
  text("What is soft \nmeans to you, and \nhow to express that?",
    20, 100);
}
```

See Allison Parrish - [fun leading tricks](https://editor.p5js.org/allison.parrish/sketches/yhLAp-tSo)

### ✨2.6 text & font

💻RunMe: https://editor.p5js.org/siusoon/sketches/ypDbHlV_C

```javascript
function setup() {
  createCanvas(400, 400);
}
function draw() {
  background(255,255,0);
  fill(255,0,255);
  //sans-serif, monospace, serif
  textFont("monospace");
  textSize(8 + mouseX);
  text("Hello World!", 10, 200);
}
```

#### 2.7 Custom font:

💡note: web font format - otf, ttf

**Example:**

0. Found a font online
1. Download the font [PicNic](https://www.velvetyne.fr/fonts/picnic/download/) by Marielle Nils
2. Unzip the file
3. Upload the oft font (e.g PicNic-Regular.otf) on P5.web editor - left panel "Sketch Files" (must be less than 5MB)

💻RunMe:  https://editor.p5js.org/siusoon/sketches/4rBn4mlTG

```javascript
let fPicNic;

function preload() {
  fPicNic = loadFont('PicNic-Regular.otf');
}

function setup() {
  createCanvas(400, 400);
}
function draw() {
  background(255,255,0);
  fill(255,0,255);
  //sans-serif, monospace, serif
  textFont(fPicNic);
  textSize(8 + mouseX);
  text("Hello World!", 10, 200);
  textFont("monospace");
  textSize(8 + mouseX);
  text("Hello World!", 10, 300);
}
```

💡Note:
- `Preload()` - load external files e.g font, audio, images, video, etc. The program will wait until the files are loaded before executing the setup function. (we have loadFont, loadImage, etc), see ref [here](https://p5js.org/reference/#/p5/preload)
- set a global variable of the font
- use the font with `textFont();`

🔖resource:
- [Web safe fonts](https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_text/Fundamentals#web_safe_fonts)
- [The League of MovableType: Open Source Fonts](https://www.theleagueofmoveabletype.com/)
- [Velvetyne Type Foundry: Open Source Fonts](https://www.velvetyne.fr)

### 👾2.8 in-class ex 2:

Try to run over again the sample sketches and make a sketch which contains a word, or a statement, that expresses your feeling right now.

---

## 3 text(string) arrays

💻RunMe: https://editor.p5js.org/siusoon/sketches/e8sChDSdu

```javascript
let terms = ["soft", "comfortable", "energised", "weird", "down", "good", "confused"];

function setup() {
  createCanvas(400, 400);
  frameRate(1);
}
function draw() {
  background(255,255,0);
  fill(255,0,255);
  //sans-serif, monospace, serif
  textFont("monospace");
  textSize(18) ;
  text("I am feeling "+ random(terms), 10, 200);
}
```

See the artwork: [Mexicans in Canada](http://amiraha.com/mexicansincanada/) by Amira Hanafi (2020).

💡note:
- array: a list of data and is related to previous concepts such as variable (storage)
- you can add (concatenate) different words together

### 3.1 click to add text

💻RunMe: https://editor.p5js.org/siusoon/sketches/vY_3fgSmf

```javascript
let terms = ["soft", "comfortable", "energised", "weird", "down", "good", "confused"];

let ySpace = 20;

function setup() {
  createCanvas(400, 400);
  background(255,255,0);
}

function mousePressed(){
  fill(255,0,255);
  //sans-serif, monospace, serif
  textFont("monospace");
  textSize(13) ;
  text("I am feeling "+ random(terms), 10, ySpace);
  ySpace+=20; //ySpace = ySpace + 20
}
```

## 4. Queering Damage

> QUEERING DAMAGE is an analytic to read-write the complexity of damaged/damaging technosciences and to situate ourselves in these mundane and alluring, yet reoccurring scenes. It is a set of operations that can be practiced collectively to consider the agencies, labours and harms of technoscience, to imagine life otherwise.  (The Underground Division - Queering Damage)

> Instead of extending benevolent utopianism, this praxis extends queer theories that concern personal injury into more-than-human ensembles, in order to consider the shared damages. (The Underground Division - Queering Damage)

- 👀look into feminist manifestos/poetry, any of your favorite piece?

- 👀 👾Develop a glossary

examples of terms:
queer - agencies - damage - injury - environment - nature - pain - inclusion - diversity - oppression - harm, etc

elements to work with: 
https://queeringdamage.hangar.org/index.php/OPERATION_2

instructions: (Forumlae: "A formula is a special type of equation that shows the relationship between different variables" )

1. define your area of interest/situation in relation to locating the damage 
2. define the spacetime conditioning: e.g ephemeral, eternal, cyclic, linear, continuous, eventual, historical present, memory, extincted, dreamed, extended, distributed, dissoluted, dispersed, concentrated, catalized, centrifugal, centripetal, located, situated, regional, local, global, reachable, micro, meso, macro...) 
3. Choose a collective term and articulate it with around 100 words / point forms if you like.

Work within your group and try to come up with an articulation of a term:
https://pad.vvvvvvaria.org/SOFT

### 👾4.1 in-class ex 3:

Based on the developed glossary, or a specific term, to create a sketch

## 5. extra: insert a bg image (static/animation)

💻RunMe: https://editor.p5js.org/siusoon/sketches/g6zV73cP2

```javascript
let img;
let gif;

function preload(){
  img = loadImage("https://static.wikia.nocookie.net/villains/images/0/0c/Greycat01.png");
  gif = createImg("https://64.media.tumblr.com/51ef48f1ce3ecf9344c1e77c0be026c7/tumblr_njacw4G0Vq1r3f8v2o1_1280.gif");
}

function setup() {
  createCanvas(400, 400);
}
function draw() {
  background(255,255,0);
  image(img,20,0);
  gif.position(-300, 0);
  gif.size(640, 382);
  fill(255,0,255);
  //sans-serif, monospace, serif
  textFont("monospace");
  textSize(8 + mouseX);
  text("Hello World!", 90, 300);
}
```

✨artwork - Fitness by Diana Lindbjerg 

<img src="https://static.wixstatic.com/media/e76dc4_aa54df98a869487b91efaa6ec883e929~mv2.gif" width="500">

---

## 6 Readings/Resources/Homework

**📕Readings:**
1. [Queering Damage Manual](https://queeringdamage.hangar.org/index.php/File:Queering_damage_final22.pdf) by The Underground Division
2. (if you have time) [Postnatural Computing](https://www.dropbox.com/s/u6jp4hparntkz6e/The%20Animal%20Hacker_FINAL_5_Aug-LOWRES.pdf?dl=0) by Helen Pritchard
, The Animal Hacker, PhD dissertation, Queen Mary, University of London, 2018. (pp. 87-103)

**🔖resources:**
- [Exploding text](https://editor.p5js.org/allison.parrish/sketches/UtE3ft07l) by Allison Parrish

## 7 📖Homeworks:
- **Read** the assigned reading for tomorrow
- prepare one sound file (e.g mp3, wav format) and one image jpg file about nature that you are interested or concerned about.

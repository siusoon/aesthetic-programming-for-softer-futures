[[_TOC_]]

# Day 4 of Aesthetic Programming for SOFTer Futures

👄Thur (22.9) - 09.00-16.00: Expressing love

## 🤔 1. What does love mean in the age of computerized world?

- Can a machine feels love? Can a computer falls in love?
- What do you want to say about love?

## 👀2. See some artworks:

![](http://media.rhizome.org/blog/8951/strachey-note.jpeg)

❤️‍🔥**Love Letters (1952) by Christopher Strachey**

💻RunMe: https://nickm.com/memslam/love_letters.html

- Christopher Strachey, a mathematician and computer scientist
- ran on the Manchester Mark I
- using a random number generating algoritm
- M.U.C stood for Manchester University Computer
- syntactic skeleton: "You are my — Adjective — Substantive,” and “My — [Adjective] — Substantive — [Adverb] — Verb — Your — [Adjective] — Substantive"
- some words are fixed and some are optional
- the program selects from a list of options — adjectives, adverbs, and verbs — and loops are configured to avoid repetition.
- The software can generate over 318 billion variations.
- the dialogue structure is important in setting up an exchange between “Me” (the program writer) and “You” (human reader), so you feel personally addressed.

🔖**resource**: 
[A Queer History of Computing](https://rhizome.org/editorial/2013/feb/19/queer-computing-1/) by Jacob Gaboury (2013)

🤔 What might be your love letter?

## ✨3. e-lit: Code and/As Poetry

[Electronic Literature: What is it?](https://eliterature.org/pad/elp.html) (Hayles 2007)

> In the contemporary era, both print and electronic texts are deeply interpenetrated by code. Digital technologies are now so thoroughly integrated with commercial printing processes that print is more properly considered a particular output form of electronic text than an entirely separate medium. Nevertheless, electronic text remains distinct from print in that it literally cannot be accessed until it is performed by properly executed code. The immediacy of code to the text's performance is fundamental to understanding electronic literature, especially to appreciating its specificity as a literary and technical production. Major genres in the canon of electronic literature emerge not only from different ways in which the user experiences them but also from the structure and specificity of the underlying code. Not surprisingly, then, some genres have come to be known by the software used to create and perform them

### 3.1 codework

![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/AP2022/class09/ccs_01.png)

> We view programming as pragmatic, logical, and a tool to build useful software. Poetry on the other hand, is an expressive form of literature, gratuitously created for beauty only.
Can they mix themselves to create something more? -- Élise Duverdier

🤔Can we see computer source code as a form of poetry?

- code with symbols (language structure) and as poetry: read aloud (poetry reading)
- code is both script and performance -> ready to do something
- analogy to speech (do things with words + code)
- Voice: instability - human subject

### 3.2 Queer Code

- Coding source code and critical writing operate together
- requires a different kind of reading & writing
- expose the material and linguistic tensions between writing, coding and reading
- question the hegemonic view of computing
- critique of binary thinking

### ✨3.3 key concepts

- constraints - syntax - execution - poetics - story telling

#### 👾3.4 in-class ex 1:

READ this together 👉🏽https://criticalcode.recipes/contributions/the-console-log-and-ways-of-being

> This contribution is dedicated to young QTBIPOC, immigrant, and disabled artists, makers, students, and educators delving into a new-found relationship with code and desire to envision and center their aspirations and dreams of what code can and ought to be! I wanted to conclude with the literary inspirations for this recipe. -Dorothy R. Santos

**STEPS:**
1. We will focus on p5.js
2. Select a library e.g p5.sound, p5.js
3. Read the provided descriptions for each chosen function and Select between 5-10 functions and create experimental prose or poetry within your library of choice

## 👀4. See some artworks:

- [Microcodes](http://pallthayer.dyndns.org/microcodes/) by Pall Thayer
- [Class](http://yoha.co.uk/node/514) by Graham Harwood (2008)
- [Vocable Code](https://siusoon.net/projects/vocablecode) by Winnie Soon (2017)
- [Code Contract: Flight delay](https://siusoon.net/projects/codecontract-flightdelay) by Winnie Soon & Megan Ma (2022)
- [forkonomy() certificate](https://gitlab.com/siusoon/forkonomy/-/blob/master/certificate.js) by Winnie Soon & Lee Tzu Tung (2021)

## 5. Reading time...- Vocable Code 

RunMe: https://dobbeltdagger.net/VocableCode/

ReadMe:Vocable Code by Winnie Soon & Geoff Cox, https://aesthetic-programming.net/pages/7-vocable-code.html


### 👾5.1 in-class ex 2:

Create your code poetry with your own naming of variables, arrays, functions, and selected syntaxes

## 6. Readings/Resources/Final Project

**📕Readings:**
- Vocable Code by Winnie Soon & Geoff Cox, https://aesthetic-programming.net/pages/7-vocable-code.html
- Auto generator by Winnie Soon & Geoff Cox, https://aesthetic-programming.net/pages/5-auto-generator.html

**🔖resource**
- code::art journal: https://code-art.xyz/issues/
- [code {poems}](code {poems} is a project by Ishac Bertran) by Ishac Bertran

### 👾6.1 Final Project Brief

https://gitlab.com/siusoon/aesthetic-programming-for-softer-futures/-/tree/main/#final-project-brief

#### 6.2 Studio time and concept/sketch presentation

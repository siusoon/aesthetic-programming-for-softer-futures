# 😻Aesthetic Programming for SOFTer Futures
![](https://gitlab.com/aesthetic-programming/book/-/raw/master/cover/AP_coverGraph.jpg)

[[_TOC_]]

## 🐚 Basic Info

https://vorlesungsverzeichnis.hgk.fhnw.ch/#/
@ A 2.13

### 🌻Programme: CoCreate Programme (2 ECTS)

Workload: 30h contact study and 30h self study  

### 📔Description

This one-week intensive course introduces coding otherwise with a focus on cultural and collective imaginaries of programming for SOFTer futures, which is more gentle, fluid, healing, loving and intimate than corporate systems of surveillance, standardisation, optimisation and exploitation, where one can reflect, express, question, imagine and explore cultural life forms.

Building upon Aesthetic Programming (Soon & Cox 2020), the course covers the building blocks of code language processing as well as explores the history of generative poetics, such as love letters, poem generators and code poetry, as a way to shift our focus from mere technical skills to wider social-aesthetic relations addressing gender disparity, societal issues and social/environmental justice in digital culture. The notion of SOFTer Futures is inspired by many organisation, artists and practitioners who put empathy, care, love and community as a priority, for example, Digital Love Languages by Melanie Hoff, Softer Digital Futures by Ida Lissner and Nicole Jonasson, as well as Queering Damage by The Underground Division.

No prior coding experience necessary and all levels welcome. The course will use JavaScript as the main programming language with a primary focus on the open source and web-based library called p5.js (https://p5js.org/), which already comes with a comprehensive and accessible resource for beginners to learn programming. Aesthetic Programming combines the acquisition of technical knowledge to do critical work for SOFTer Futures, all part of learning to program in its widest sense, programming as doing and thinking, as world-making.

### 🎈Competencies

- The students know basic programming and are able to discuss wider socio-technical-aesthetic relations.
- They are able to build critical software programs with empathy, care and love.

### 🎏attendance requirement:

- Capacity: 18
  - We have: 18 students from Industrial Design, Arts and Design Education, Fine Arts, Process Design, Visual Communication and Digital Spaces and Interior Architecture and Scenography
- at least 80% attendance
- Pass/Fail (see the final project brief in the last class)
  1. Produce a RUNME (critical software artefact) and README as a group project
  2. Presenting/exhibition the group final work on Friday

### 😎 Who is Winnie

Winnie Soon is an artistic coder and researcher concerning human rights and social justice within the context of computational culture. With works appearing in museums/galleries, festivals, distributed networks, papers/books, they are the author of two books titled “[Aesthetic Programming: A Handbook of Software Studies](https://aesthetic-programming.net/)” (w/ Geoff Cox) and “[Fix My Code](https://eeclectic.de/produkt/fix-my-code/)” (w/ Cornelia Sollfrank). They are currently based in Denmark and London, working as Associate Professor at Aarhus University and visiting researcher at London South Bank University. More: [siusoon.net](https://siusoon.net/)

**✌️Motto:**
```javascript
function start() {
  setInterval(your_motto, now);
}
```

## 📕Literature

-	Soon, Winnie, and Geoff Cox. [Aesthetic programming: A Handbook of Software Studies](https://aesthetic-programming.net/). Open Humanities Press, 2020.
-	Vee, Annette. Coding literacy: How computer programming is changing writing. MIT Press, 2017.

### 🔖learning resource

- [Class sketches](https://editor.p5js.org/siusoon/collections/zcD9tWrlt)
- [Intro to Programming in p5.js](https://www.youtube.com/watch?v=wiG7wLwyW0E&list=PLT233rQkMw761t_nQ_6GkejNT1g3Ew4PU) by Xin Xin
- [The Coding Train](https://thecodingtrain.com/) by Daniel Shiffman

## temp multimedia storage space

Github/Gitlab
or
FTP: guest@xxxxxxxxxx.net

## 💡 Schedule

### Pre-class prep (everyone)

1. 👀 **READ** this short article: [Aesthetic Programming: A Queer Praxis in P5.js](https://enginesofdifference.org/2021/01/05/aesthetic-programming-a-queer-praxis-in-p5-js-1/)
2. **Select and Read** any one of the articles from the blog [engines of difference](https://enginesofdifference.org/) edited by Loren Britton and Nana Kesewaa, then:
  - 🤔 Think: Which sentence(s) or idea(s) catches your attention and/or inspires your thinking, why?
  - 💄 Do something wild: Make an animated gif (within 5 seconds and less than 5 MB) of your chosen scene/object/things (can be analogue or digital or blended mode) that describe your inspiration/attention
3. 🤓 Look around the software library: [p5.js](https://p5js.org/)

---

### 🧠[Mon (19.9) - 13.00-16.00](day01.md)

⏰13.00-14.00:
Welcome in "Aula D 1.04" by Nicolaj van der Meulen and Sophie Stephanie Kellner

⏰14.00-16.00:

- 😀Who + What + Learn (Aesthetic + Programming + for + SOFTer + futures)
- 👶Knowing each other (true and not so true)
- ✨Coding Otherwise - the notion of SOFTer Futures
  - ❤️‍[Digital Love Languages](https://lovelanguages.melaniehoff.com/) by Melanie Hoff
  - ❤️‍[Softer Digital Futures](https://softerdigitalfutures.xyz/) by [Softer](https://softer.website/)(Ida Lissner and Nicole Jonasson)
  - ❤️‍[Queering Damage](https://queeringdamage.hangar.org/index.php/Main_Page) by The Under-ground Division
- 💻p5.js and the web editor
- 📖Homework: Forming groups & light readings

---

### 👁[Tue (20.9) - 09.00-16.00: Queering damage](day02.md)

- Basics (coordinate system, drawing)
- Reading the refernece guide
- Textuality, text alignment, text in a box, text leading, text & font
- text(string) arrays
- click to add text
- Queering Damage
- extra: insert a bg image (static/animation)
- 📖Homework: light readings + prepare files about nature

---

### 👂[Wed (21.9) - 09.00-16.00: Caring nature](day03.md)

- What constitues nature?
- Some artworks
- Deconstructing A House of Dust
- A House of Dust in p5.js
- data, variable, arrays
- conditional statement, random, loops, transformation
- Haiku Poetry
- Feminist materialisms
- Going to the wild
- Examples of e-lit works
- Extra: Click to play sound/audio, Adding background video
- 📖Homework: light readings + highlights

---

### 👄[Thur (22.9) - 09.00-16.00: Expressing love](day04.md)

- 🤔 What does love means in the age of computerized world?
- ❤️Sharing of readings
- 👀See some artworks: Love letters, Microcodes, Class, Vocable Code, Code Contract: Flight delay, Forkonomy() certificate
- e-lit: Code and/As Poetry
- codework
- Queer code
- key concepts
- Prep for final project

---

### 💪Fri (23.9) - Studio Day

#### 👾Final project brief (as a group)
1. Produce a software package (includes both ReadMe and RunMe) that respond to the notion of SOFTer futures.
  - RUNME: Program an artifact with p5.js (or a combination of HTML, CSS, Javascript) to produce a sketch (can be in the form of code poetry, generative art, audio-visual media, etc)
  - README (1x A4): Write the description of the artefact and the related concepts/stories within 300 words. The readme should includes a URL (to run your program), and a screenshot image.
  - Prepare a 2 min elevator pitch of your project.

#### schedule
- ⏰09.00-11.00 Studio - fine tuning / polishing / framing
- ⏰11.00-12.00 In-class presentation / peer critique
- ⏰13.00-14.00 Preparation at CIVIC
- ⏰14.00-16.00 presentation + exhibition + elevator pitch

---

### students' work 

- Luisa:  https://editor.p5js.org/Luisa042/full/w2_P91nX2 
- Do you feel accepted by Marla Asta, Zhenya Semenova, Varvara Prokopiv  https://editor.p5js.org/VaryaP/full/iJrXfyp2p
- Working_bodies by no.plan: https://editor.p5js.org/no.plan/full/jjkGRo0V2
- Damage=Change by Diana: https://editor.p5js.org/siusoon/full/zCcpWLg6R

